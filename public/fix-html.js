document.addEventListener('DOMContentLoaded', () => {
  const style = document.createElement('style');
  style.textContent = 'body { visibility: hidden; }';
  document.head.appendChild(style);

  Promise.all([...document.getElementsByTagName("include")].map(i =>
    fetch(i.getAttribute("src"))
      .then(res => res.text())
      .then(content => {
        i.insertAdjacentHTML("afterend", content);
        i.remove();
      })
  )).then(() => {
    style.remove();
    document.body.style.visibility = "visible";
  });
});
